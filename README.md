## PicoBF: A simple, interactive Brainfuck interpreter

PicoBF is a compact Brainfuck interpreter designed for expandability. Its name
is inspired by the C interpreter [PicoC](https://gitlab.com/zsaleeba/picoc).

 - [Latest stable release](https://gitlab.com/ryukoposting/PicoBF/tags/0.0)
 - [All releases](https://gitlab.com/ryukoposting/PicoBF/tags)

### Things To Know:
- **PicoBF has an interactive mode.** The directive for the interactive mode is `-i`.
- **PicoBF reads its parameters serially,** without resetting the Brainfuck runtime
  in between files or interactive instances. This means that `picobf foo.bf bar.bf`
  and `picobf bar.bf foo.bf` are NOT the same thing.
  - Say you call `picobf foo.bf bar.bf`. foo.bf's tape pointer will
    start at 0, and all initial values in the tape will be 0. bar.bf will
    start wherever foo.bf left off; bar.bf's tape pointer will start wherever
    foo.bf left its tape pointer, and the data initially in bar.bf's tape will
    e the same as what foo.bf left in its tape.

- **Interactive mode instances can be mixed with calls to files.** For example,
  `picobf foo.bf -i` will run foo.bf, then enter interactive mode. As with calls
  to multiple files, interactive mode will start with the same tape and tape
  pointer left behind when foo.bf finished running.

- **PicoBF uses a hard-coded tape of length 2048,** and the tape pointer starts at
  0 (the leftmost position on the tape). This is subject to change in the
  future (adding negative tape indices, most likely).

- **In interactive mode, PicoBF will display runtime errors for certain actions
  without stopping interactive runtime.** This includes attempts to move the tape
  pointer out-of-bounds, as well as mismatched brackets.
  - Example: If the program attempts to move the tape pointer out of bounds, a
    runtime error will be shown but the program will continue, leaving the tape
    pointer at the boundary that was hit. For example, `+[>+]` will attempt to exit
    the right boundary of the tape, but PicoBF will stop it at the rightmost
    tape index, and show the user a runtime error, but the user will be
    allowed to continue in interactive mode from the last legal tape position
    the program attempted to access.

### lib-picobf

The entire PicoBF interpreter is wrapped in a CMake Library, so that it can be
easily imported into other projects. The main.c included in this repo does none
of the actual work- it is simply a breakout for lib-picobf.
