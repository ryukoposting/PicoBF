#include <stdio.h>
#include <string.h>
#include "lib-picobf/picobf.h"
#include "lib-picobf/runtime/brainfuck.h"

int main(int argc, char* argv[]) {
    PicoBF_Init();

    PicoBF_ParseArgs(argc, argv);

    printf("Brainfuck runtime finished with value %d at position %d", Brainfuck_Get(), Brainfuck_TapePos());

    Brainfuck_Exit();
    PicoBF_Exit();
    return 0;
}
