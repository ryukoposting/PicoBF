#include "brainfuck.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int Brainfuck_Init(unsigned int tape_size) {
    if(tape_ != NULL) return -1;
    tape_size_ = tape_size;
    tape_pointer_ = 0;
    tape_ = calloc(tape_size, sizeof(char));
}

int Brainfuck_Parse(char* bf_string, unsigned int current_index) {
    if((current_index < 0) || (current_index >= strlen(bf_string))) {
        return tape_[tape_pointer_];
    }

    char cmd = bf_string[current_index];
    if(cmd < 0) return tape_[tape_pointer_];

    switch(cmd) {
        case '>':
            if(++tape_pointer_ >= (tape_size_ - 1)) {
                printf("Runtime error at index %d (%c): tape pointer out of bounds\n", current_index, cmd);
                return Brainfuck_Parse(bf_string, -1);
            }
            break;
        case '<':
            if(tape_pointer_ == 0) {
                printf("Runtime error at index %d (%c): tape pointer out of bounds\n", current_index, cmd);
                return Brainfuck_Parse(bf_string, -1);
            } else {
                --tape_pointer_;
            }
            break;
        case '+':
            ++tape_[tape_pointer_];
            break;
        case '-':
            --tape_[tape_pointer_];
            break;
        case '.':
            printf("%c", tape_[tape_pointer_]);
            break;
        case ',':
            do {
                scanf("%c", &tape_[tape_pointer_]);
            } while(tape_[tape_pointer_] == '\n');
            break;
        case '[':
            if(bracket_pointer_ == 63) {
                printf("Runtime error at index %d (%c): no more than 64 nested brackets is permitted\n", current_index, cmd);
                return Brainfuck_Parse(bf_string, -1);
            }
            // optimize the clear and find 0 idioms
            if(current_index < (strlen(bf_string) - 2)) {
                if((bf_string[current_index + 1] == '-') && (bf_string[current_index + 2] == ']')) {
                    tape_[tape_pointer_] = 0;
                    return Brainfuck_Parse(bf_string, current_index + 3);
                }
                if((bf_string[current_index + 1] == '>') && (bf_string[current_index + 2] == ']')) {
                    do {
                         if(tape_[tape_pointer_] == 0) return Brainfuck_Parse(bf_string, current_index + 3);
                    } while(++tape_pointer_ < tape_size_);
                    printf("Runtime error at index %d (>): tape pointer out of bounds\n", current_index + 1);
                    return Brainfuck_Parse(bf_string, -1);
                }
                if((bf_string[current_index + 1] == '<') && (bf_string[current_index + 2] == ']')) {
                    do {
                         if(tape_[tape_pointer_] == 0) return Brainfuck_Parse(bf_string, current_index + 3);
                    } while(--tape_pointer_ > 0);
                    printf("Runtime error at index %d (<): tape pointer out of bounds\n", current_index + 1);
                    return Brainfuck_Parse(bf_string, -1);
                }
            }
            bracket_positions_[++bracket_pointer_] = current_index;
            if(tape_[tape_pointer_] == 0) { // find matching close bracket
                for(unsigned int i = current_index; i < strlen(bf_string); i++) {
                    if(bf_string[i] == ']') return Brainfuck_Parse(bf_string, i + 1);
                }
                printf("Runtime error at index %d (%c): open bracket has no match\n", current_index, cmd);
                return Brainfuck_Parse(bf_string, -1);
            }
            break;
        case ']':
            if(bracket_pointer_ < 0) {
                printf("Runtime error at index %d (%c): close bracket has no match\n", current_index, cmd);
                return Brainfuck_Parse(bf_string, -1);
            } else {
                bracket_pointer_--;
                if(tape_[tape_pointer_] != 0) {
                    return Brainfuck_Parse(bf_string, bracket_positions_[bracket_pointer_ + 1]);
                }
            }
    }
    return Brainfuck_Parse(bf_string, current_index + 1);
}

char Brainfuck_Get() {
    return tape_[tape_pointer_];
}

unsigned int Brainfuck_IsInitialized() {
    return tape_size_ != 0;
}

unsigned int Brainfuck_TapePos() {
    return tape_pointer_;
}

void Brainfuck_Exit() {
    tape_size_ = 0;
    free(tape_);
}
