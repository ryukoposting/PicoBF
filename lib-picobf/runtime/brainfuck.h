#ifndef BRAINFUCK_H_
#define BRAINFUCK_H_

static char* tape_;
static unsigned int tape_size_ = 0;
static unsigned int tape_pointer_;
static unsigned int bracket_positions_[64] = {};
static int bracket_pointer_ = -1;

int Brainfuck_Init(unsigned int tape_size);

/*
 * Will return the index of the next char to be parsed.
 */
int Brainfuck_Parse(char* bf_string, unsigned int current_index);

/*
 * Get the value of the char under the tape pointer
 */
char Brainfuck_Get();

unsigned int Brainfuck_IsInitialized();

unsigned int Brainfuck_TapePos();

void Brainfuck_Exit();

#endif
