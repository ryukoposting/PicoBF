#ifndef PICOBF_H_
#define PICOBF_H_

#define PICOBF_COMMAND_ARRAY_SIZE 3

#include "runtime/brainfuck.h"
#include "command/command.h"
#include "command/load_from_file.h"
#include "command/get_version.h"
#include "command/interactive.h"

static Command * command_array_[PICOBF_COMMAND_ARRAY_SIZE];

void PicoBF_Init(void);

int PicoBF_ParseArgs(int argc, char* argv[]);

void PicoBF_Exit(void);

#endif  // PICOBF_COMMAND_COMMAND_H_
