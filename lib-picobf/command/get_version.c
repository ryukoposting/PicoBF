#include "get_version.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int GetVersion_IsValid(char* arg) {
    if(strlen(arg) != 2) return 0;
    return (arg[0] == '-') && (arg[1] == 'v');
}

int GetVersion_Execute(char* arg) {
    printf("\n PicoBF version %d.%d\n\n", LIBPICOBF_VERSION_MAJOR, LIBPICOBF_VERSION_MINOR);
    printf(" Made by ryukoposting\n gitlab.com/ryukoposting\n ryukoposting.tumblr.com\n\n");
    printf(" Licensed under GNU GPL v3\n");
    exit(0);
    return -1;
}
