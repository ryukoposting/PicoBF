#ifndef PICOBF_COMMAND_LOAD_FROM_FILE_H_
#define PICOBF_COMMAND_LOAD_FROM_FILE_H_

#include "command.h"

int LoadFromFile_IsValid(char* arg);

int LoadFromFile_Execute(char* arg);

#endif
