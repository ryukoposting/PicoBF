#include "command.h"

#include <stdlib.h>

Command *NewCommand(char* name, char* helpmsg, int (*isvalid)(char*), int (*execute)(char*)) {
    Command *new_command = malloc(sizeof(Command));

    new_command->name = name;
    new_command->helpmsg = helpmsg;
    new_command->IsValid = isvalid;
    new_command->Execute = execute;

    return new_command;
}
