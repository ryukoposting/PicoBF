#ifndef PICOBF_COMMAND_H_
#define PICOBF_COMMAND_H_

typedef struct Command_t {
    char * name;
    char * helpmsg;
    int (*IsValid)(char*);
    int (*Execute)(char*);
} Command;

Command *NewCommand(char* name, char* helpmsg, int (*isvalid)(char*), int (*execute)(char*));

#endif
