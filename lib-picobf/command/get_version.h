#ifndef PICOBF_COMMAND_GET_VERSION_H_
#define PICOBF_COMMAND_GET_VERSION_H_

#include "command.h"
#include "..\config.h"

int GetVersion_IsValid(char* arg);

int GetVersion_Execute(char* arg);

#endif
