#include "get_version.h"

#include <stdio.h>
#include <string.h>

#include "..\runtime\brainfuck.h"

int Interactive_IsValid(char* arg) {
    if(strlen(arg) != 2) return 0;
    if((arg[0] == '-') && (arg[1] == 'i')) {
        printf("type 'exit' into the interactive prompt to exit.\n");
        if(!Brainfuck_IsInitialized()) {
            Brainfuck_Init(2048);
        }
        return 1;
    }
}

int Interactive_Execute(char* arg) {
    char instr[512];
    do {
        printf("\n[%d] = %d\n", Brainfuck_TapePos(), Brainfuck_Get());
        printf("> ");
        scanf("%s", instr);
        Brainfuck_Parse(instr, 0);
    } while(strcmp(instr, "exit"));
}
