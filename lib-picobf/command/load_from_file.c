#include "load_from_file.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "..\runtime\brainfuck.h"

int LoadFromFile_IsValid(char* arg) {
    char* extension = strrchr(arg, '.');
    if(extension == NULL) return 0;
    return (strcmp(extension, ".bf") == 0) | (strcmp(extension, ".b") == 0) | (strcmp(extension, ".brainfuck") == 0);
}

int LoadFromFile_Execute(char* arg) {
    char * buffer = 0;
    long length;
    FILE * f = fopen (arg, "rb");

    if(f) {
      fseek(f, 0, SEEK_END);
      length = ftell (f);
      fseek(f, 0, SEEK_SET);
      buffer = malloc(length);
      if(buffer) {
        fread(buffer, 1, length, f);
      }
      fclose(f);
    } else {
      printf("File not found: %s\n", arg);
    }

    if(buffer) {
        if(!Brainfuck_IsInitialized()) {
            Brainfuck_Init(2048);
        }
        Brainfuck_Parse(buffer, 0);
    }
    return 0;
}
