#ifndef PICOBF_COMMAND_INTERACTIVE_H_
#define PICOBF_COMMAND_INTERACTIVE_H_

#include "command.h"
#include "..\config.h"

int Interactive_IsValid(char* arg);

int Interactive_Execute(char* arg);

#endif
