#include "picobf.h"

#include <stdio.h>
#include <stdlib.h>

void PicoBF_Init(void) {
    command_array_[0] = NewCommand("<file.bf> / <file.b> / <file.brainfuck>", "Load a Brainfuck file.", &LoadFromFile_IsValid, &LoadFromFile_Execute);
    command_array_[1] = NewCommand("-v", "Show PicoBF version and credits.", &GetVersion_IsValid, &GetVersion_Execute);
    command_array_[2] = NewCommand("-i", "Enter PicoBF interactive mode.", &Interactive_IsValid, &Interactive_Execute);
    //command_array_[1]->Execute("foo");
}

int PicoBF_ParseArgs(int argc, char* argv[]) {
    if(argc == 1) {
        for(unsigned int j = 0; j < PICOBF_COMMAND_ARRAY_SIZE; j++) {
            printf("\n%s\n    %s\n", command_array_[j]->name, command_array_[j]->helpmsg);
        }
        printf("\nAll brainfuck files and interactive mode calls are loaded serially\n");
        printf("into the same brainfuck runtime. This means that, if two brainfuck\n");
        printf("files are called, the second one will start with the same tape\n");
        printf("values and tape pointer left behind by the first one.\n");
        exit(0);
    }
    for(unsigned int i = 1; i < argc; i++) {
        // try to find a matching command
        unsigned short notConsumed = 1;
        for(unsigned int j = 0; j < PICOBF_COMMAND_ARRAY_SIZE; j++) {
            if(command_array_[j]->IsValid(argv[i])) {
                command_array_[j]->Execute(argv[i]);
                notConsumed = 0;
            }
        }
        // no matching commands, show help
        if(notConsumed) {
            printf("error: unknown argument: %s\n", argv[i]);
            for(unsigned int j = 0; j < PICOBF_COMMAND_ARRAY_SIZE; j++) {
                printf("%s\n    %s\n\n", command_array_[j]->name, command_array_[j]->helpmsg);
            }
            exit(0);
        }
    }
    return 0;
}

void PicoBF_Exit(void) {
    for(unsigned int i = 0; i < (sizeof(command_array_) / sizeof(command_array_[0])); i++) {
        free(command_array_[i]);
    }
}
